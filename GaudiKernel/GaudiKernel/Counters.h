#include <GAUDI_VERSION.h>
#if GAUDI_VERSION >= CALC_GAUDI_VERSION( 33, 0 ) && GAUDI_MAJOR_VERSION < 999
#  pragma message "warning: deprecated header, use <Gaudi/Accumulators.h> instead"
#endif
#include <Gaudi/Accumulators.h>
